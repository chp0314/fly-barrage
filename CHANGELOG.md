## 2.1.0 (2024-02-27)
### Features
* 增加离屏 Canvas 性能优化 ([feature-1](https://github.com/feiafei27/fly-barrage/tree/feature-1))

## 2.0.0 (2024-02-21)
第一个版本
